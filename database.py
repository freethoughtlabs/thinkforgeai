from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from fastapi import FastAPI
from fastapi.requests import Request
from fastapi.responses import Response
from sqlalchemy.orm import Session
from config import Config
from models import Base

DATABASE_URI = Config.DATABASE_URI

engine = create_async_engine(DATABASE_URI, echo=True, future=True)

async_session = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)
Request.state = Session

'''
Function create_tables() creates all the tables defined in the Base metadata of 
the models module by using the async_session context manager that returns an 
AsyncSession instance to execute DDL SQL statements asynchronously.
'''
async def create_tables():
    async with async_session() as session:
        async with session.begin():
            await session.run_sync(Base.metadata.create_all)

'''
Function get_db() returns an AsyncSession instance from the async_session factory. 
This instance is used as a context manager to execute DML SQL statements asynchronously.
'''
async def get_db():
    async with async_session() as session:
        yield session

'''
Function configure_database(app: FastAPI) is a middleware function that configures 
the app to use a session from the async_session factory when handling HTTP requests. 
It does so by setting the current AsyncSession instance as an attribute of the current 
request object, allowing route functions to access the database connection. The 
middleware function ensures that the session is committed after each request, and in 
case of an exception, the session is rolled back. Finally, the session is closed.
'''
def configure_database(app: FastAPI):
    @app.middleware("http")
    async def commit_transactions(request: Request, call_next):
        response = Response(status_code=500)

        async with async_session() as session:
            request.state.db = session  # Changed this line
            try:
                response = await call_next(request)
                await session.commit()
            except Exception:
                await session.rollback()
                raise
            finally:
                await session.close()

        return response
