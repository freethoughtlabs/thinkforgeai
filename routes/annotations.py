from datetime import datetime
from typing import List, Dict

from fastapi import APIRouter, Response, HTTPException, Depends, status
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from database import get_db
from models import Annotation, Comment
from schemas import (
    AnnotationCreate,
    AnnotationRead,
    AnnotationUpdate,    
)

annotations_router = APIRouter()

# The get_annotations() route returns all annotations present in the database.
@annotations_router.get('/', response_model=List[AnnotationRead])
def get_annotations(db: Session = Depends(get_db)):
    annotations = db.query(Annotation).all()
    return annotations

# The get_annotations_by_task_id() route returns annotations based on a task id.
@annotations_router.get('/{task_id}', response_model=List[AnnotationRead])
def get_annotations_by_task_id(task_id: int, db: Session = Depends(get_db)):
    annotations = db.query(Annotation).filter_by(task_id=task_id).all()
    return annotations

# The get_annotation() route returns a single annotation by id.
@annotations_router.get('/id/{annotation_id}', response_model=AnnotationRead)
def get_annotation(annotation_id: int, db: Session = Depends(get_db)):
    annotation = db.query(Annotation).get(annotation_id)
    if not annotation:
        raise HTTPException(status_code=404, detail=f"No annotation found with ID {annotation_id}")
    return annotation

# The create_annotation() route creates a new annotation in the database.
@annotations_router.post('/', response_model=AnnotationRead, status_code=201)
def create_annotation(annotation: AnnotationCreate, db: Session = Depends(get_db)):
    annotation_obj = Annotation(**annotation.dict(), created_at=datetime.utcnow())
    db.add(annotation_obj)
    db.commit()
    db.refresh(annotation_obj)
    return annotation_obj

# The update_annotation() route updates an existing annotation in the database.
@annotations_router.put('/{annotation_id}', response_model=AnnotationRead)
def update_annotation(annotation_id: int, annotation: AnnotationUpdate, db: Session = Depends(get_db)):
    annotation_obj = db.query(Annotation).filter(Annotation.id == annotation_id).first()
    if not annotation_obj:
        raise HTTPException(status_code=404, detail=f"No annotation found with ID {annotation_id}")
    for field in annotation.dict(exclude_unset=True):
        setattr(annotation_obj, field, annotation.dict()[field])
    annotation_obj.updated_at = datetime.utcnow()
    db.commit()
    db.refresh(annotation_obj)
    return annotation_obj

#The delete_annotation() route deletes an existing annotation from the database.
@annotations_router.delete('/{annotation_id}', status_code=status.HTTP_204_NO_CONTENT)
def delete_annotation(annotation_id: int, db: Session = Depends(get_db)):
    annotation_obj = db.query(Annotation).filter(Annotation.id == annotation_id).first()
    if not annotation_obj:
        raise HTTPException(status_code=404, detail=f"No annotation found with ID {annotation_id}")
    db.delete(annotation_obj)
    db.commit()
'''
Since we are returning the deleted annotation object, FastAPI might not be able to serialize it properly. 
To avoid any issues, you can return an empty response with a status code of 204 No Content instead.
'''

# The add_comment() route adds a new comment to an existing annotation in the database.
@annotations_router.post('/{annotation_id}/comments', response_model=AnnotationRead)
def add_comment(annotation_id: int, comment: str, db: Session = Depends(get_db)):
    annotation_obj = db.query(Annotation).filter(Annotation.id == annotation_id).first()
    if not annotation_obj:
        raise HTTPException(status_code=404, detail=f"No annotation found with ID {annotation_id}")
    new_comment = Comment(comment=comment)
    annotation_obj.comments.append(new_comment)
    db.commit()
    db.refresh(annotation_obj)
    return annotation_obj

'''
In the routes/annotations.py file, annotations and annotation_obj refer to different things.

`annotations` refers to a list of Annotation objects retrieved from the database using a query. 
This is used in the get_annotations() and get_annotations_by_task_id() functions.

`annotation_obj` refers to a single Annotation object retrieved from the database using its id. 
This is used in the create_annotation(), update_annotation(), delete_annotation(), and 
add_comment() functions.

The difference is that annotations is a list of objects, whereas annotation_obj refers to a 
single object.
'''