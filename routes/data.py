''' 
The code handles the following operations:
    Creating a new data source
    Reading all data sources with optional skip and limit parameters
    Reading a specific data source by its ID
    Updating a specific data source by its ID
    Deleting a specific data source by its ID
'''

from fastapi import APIRouter, Depends, HTTPException, Response
from sqlalchemy.orm import Session
from typing import List

from database import get_db
from models import DataSource
from schemas import (
    DataSourceRead,
    DataSourceCreate,
    DataSourceUpdate
)

data_router = APIRouter()

# create a new data source
@data_router.post("/data-sources", response_model=DataSourceRead)
async def create_data_source(data_source: DataSourceCreate, db: Session = Depends(get_db)):
    db_data_source = DataSource(name=data_source.name)
    db.add(db_data_source)
    db.commit()
    db.refresh(db_data_source)
    return db_data_source


# get all data sources
@data_router.get("/data-sources", response_model=List[DataSourceRead])
async def read_data_sources(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    data_sources = db.query(DataSource).offset(skip).limit(limit).all()
    return data_sources


# get a data source by id
@data_router.get("/data-sources/{data_source_id}", response_model=DataSourceRead)
async def read_data_source(data_source_id: int, db: Session = Depends(get_db)):
    db_data_source = db.query(DataSource).filter(DataSource.id == data_source_id).first()
    if db_data_source is None:
        raise HTTPException(status_code=404, detail="Data source not found")
    return db_data_source


# update a data source by id
@data_router.put("/data-sources/{data_source_id}", response_model=DataSourceRead)
async def update_data_source(data_source_id: int, data_source: DataSourceUpdate, db: Session = Depends(get_db)):
    db_data_source = db.query(DataSource).filter(DataSource.id == data_source_id).first()
    if db_data_source is None:
        raise HTTPException(status_code=404, detail="Data source not found")
    for var, value in vars(data_source).items():
        setattr(db_data_source, var, value) if value else None
    db.add(db_data_source)
    db.commit()
    db.refresh(db_data_source)
    return db_data_source


# delete a data source by id
@data_router.delete("/data-sources/{data_source_id}", response_class=Response)
async def delete_data_source(data_source_id: int, db: Session = Depends(get_db)):
    db_data_source = db.query(DataSource).filter(DataSource.id == data_source_id).first()
    if db_data_source is None:
        raise HTTPException(status_code=404, detail="Data source not found")
    db.delete(db_data_source)
    db.commit()
    return Response(status_code=204)
