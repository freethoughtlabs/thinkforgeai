from fastapi import APIRouter, status, Response
from fastapi.exceptions import HTTPException
from fastapi.param_functions import Depends
from sqlalchemy.orm import Session

from models import Model
from database import get_db
from schemas import ModelCreate, ModelRead, ModelUpdate

model_router = APIRouter()


@model_router.get("/models", response_model=list[ModelRead])
async def read_models(db: Session = Depends(get_db)):
    """
    Read all models.
    """
    models = db.query(Model).all()
    return models


@model_router.post("/models", status_code=status.HTTP_201_CREATED, response_model=ModelRead)
async def create_model(model: ModelCreate, db: Session = Depends(get_db)):
    """
    Create a new model.
    """
    db_model = Model(name=model.name, description=model.description)
    db.add(db_model)
    db.commit()
    db.refresh(db_model)
    return db_model


@model_router.get("/models/{model_id}", response_model=ModelRead)
async def read_model(model_id: int, db: Session = Depends(get_db)):
    """
    Read a model by ID.
    """
    db_model = db.query(Model).filter(Model.id == model_id).first()
    if db_model is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Model not found")
    return db_model


@model_router.put("/models/{model_id}", response_model=ModelRead)
async def update_model(model_id: int, model: ModelUpdate, db: Session = Depends(get_db)):
    """
    Update a model.
    """
    db_model = db.query(Model).filter(Model.id == model_id).first()
    if db_model is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Model not found")
    db_model.name = model.name
    db_model.description = model.description
    db.commit()
    db.refresh(db_model)
    return db_model


@model_router.delete("/models/{model_id}")
async def delete_model(model_id: int, db: Session = Depends(get_db)):
    """
    Delete a model.
    """
    db_model = db.query(Model).filter(Model.id == model_id).first()
    if db_model is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Model not found")
    db.delete(db_model)
    db.commit()
    return Response(status_code=status.HTTP_204_NO_CONTENT)
