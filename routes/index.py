from fastapi import APIRouter, Request, Depends
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session
from datetime import datetime
from database import get_db

templates = Jinja2Templates(directory="templates")
index_router = APIRouter()

@index_router.get("/", response_class=HTMLResponse)
async def index(request: Request, db: Session = Depends(get_db)):
    current_date = datetime.utcnow()
    return templates.TemplateResponse("index.html", {"request": request, "date": current_date})
