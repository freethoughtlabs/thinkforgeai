from fastapi import APIRouter, Depends, HTTPException, Response, status
from sqlalchemy.orm import Session
from typing import List

from database import get_db
from models import Dataset, User
from schemas import DatasetCreate, DatasetRead, DatasetUpdate, UserRead
from routes.utils import get_current_user

dataset_router = APIRouter()

@dataset_router.get("/", response_model=List[DatasetRead])
def read_datasets(db: Session = Depends(get_db), current_user: User = Depends(get_current_user)):
    datasets = db.query(Dataset).all()
    return datasets

@dataset_router.get("/{dataset_id}", response_model=DatasetRead)
def read_dataset(dataset_id: int, db: Session = Depends(get_db), current_user: User = Depends(get_current_user)):
    dataset = db.query(Dataset).filter(Dataset.id == dataset_id).first()
    if not dataset:
        raise HTTPException(status_code=404, detail="Dataset not found")
    return dataset

@dataset_router.post("/", response_model=DatasetRead)
def create_dataset(dataset: DatasetCreate, db: Session = Depends(get_db), current_user: User = Depends(get_current_user)):
    db_dataset = Dataset(name=dataset.name, description=dataset.description, user_id=current_user.id)
    db.add(db_dataset)
    db.commit()
    db.refresh(db_dataset)
    return db_dataset

@dataset_router.put("/{dataset_id}", response_model=DatasetRead)
def update_dataset(dataset_id: int, dataset: DatasetUpdate, db: Session = Depends(get_db), current_user: User = Depends(get_current_user)):
    db_dataset = db.query(Dataset).filter(Dataset.id == dataset_id).first()
    if not db_dataset:
        raise HTTPException(status_code=404, detail="Dataset not found")
    if db_dataset.user_id != current_user.id:
        raise HTTPException(status_code=403, detail="Forbidden")
    for key, value in dataset.dict(exclude_unset=True).items():
        setattr(db_dataset, key, value)
    db.add(db_dataset)
    db.commit()
    db.refresh(db_dataset)
    return db_dataset

@dataset_router.delete("/{dataset_id}")
def delete_dataset(dataset_id: int, db: Session = Depends(get_db), current_user: User = Depends(get_current_user)):
    db_dataset = db.query(Dataset).filter(Dataset.id == dataset_id).first()
    if not db_dataset:
        raise HTTPException(status_code=404, detail="Dataset not found")
    if db_dataset.user_id != current_user.id:
        raise HTTPException(status_code=403, detail="Forbidden")
    db.delete(db_dataset)
    db.commit()
    return Response(status_code=status.HTTP_204_NO_CONTENT)
