from fastapi import APIRouter, Request, HTTPException, status, Depends
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.security import HTTPBearer
from models import User
from forms import RegistrationForm, LoginForm
from database import get_db
from sqlalchemy.orm import Session

from schemas import UserRead

http_auth = HTTPBearer()

auth_router = APIRouter()
templates = Jinja2Templates(directory="templates")

def is_authenticated(request: Request, db: Session = Depends(get_db), token: str = Depends(http_auth)):
    user_id = request.headers.get("user_id")

    if not user_id:
        return False

    user = db.query(User).filter(User.id == user_id).first()
    return user is not None

async def get_current_user(request: Request, db: Session = Depends(get_db), token: str = Depends(http_auth)):
    if is_authenticated(request, db):
        user_id = request.headers.get("user_id")
        user = db.query(User).filter(User.id == user_id).first()
        return user
    else:
        return None

@auth_router.get('/register')
async def display_register(request: Request):
    form_data = RegistrationForm()
    return templates.TemplateResponse("register.html", {"request": request, "form": form_data})

@auth_router.post('/register')
async def submit_register(request: Request, form_data: RegistrationForm = Depends(), db: Session = Depends(get_db)):
    await form_data.validate_username(db)
    await form_data.validate_email(db)
    await form_data.validate_password2()

    user = User(username=form_data.username, email=form_data.email)
    user.set_password(form_data.password)
    db.add(user)
    db.commit()
    db.refresh(user)
    request.session['user_id'] = user.id
    return {"message": "Congratulations, you are now a registered user!"}


@auth_router.get('/login')
async def display_login(request: Request, current_user: User = Depends(get_current_user)):
    if current_user:
        return {"message": "User is already authenticated."}

    form_data = LoginForm()
    return templates.TemplateResponse("login.html", {"request": request, "form": form_data})


@auth_router.post('/login')
async def submit_login(request: Request, form_data: LoginForm = Depends(), db: Session = Depends(get_db), current_user: User = Depends(get_current_user)):
    if current_user:
        return {"message": "User is already authenticated."}

    user = db.query(User).filter_by(email=form_data.email).first()
    if user is None or not user.check_password(form_data.password):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid email or password")

    request.session['user_id'] = user.id
    return {"message": "Successfully logged in."}


@auth_router.get('/logout')
async def perform_logout(request: Request, current_user: User = Depends(get_current_user)):
    if current_user and 'user_id' in request.session:
        del request.session['user_id']
    return {"message": "Successfully logged out."}
