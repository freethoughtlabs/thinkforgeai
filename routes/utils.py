from datetime import datetime
from typing import List, Dict

from fastapi import APIRouter, FastAPI, Depends, HTTPException, status
from fastapi.responses import HTMLResponse
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm import Session
from fastapi.templating import Jinja2Templates
from fastapi import Request

from models import User
from schemas import UserRead
from database import get_db
import jwt
import config

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="your_token_url")
utils_router = APIRouter()

templates = Jinja2Templates(directory="templates")

# Function to format a datetime object to a human-readable string
def format_date(value, format='%Y-%m-%d %H:%M:%S'):
    if isinstance(value, str):
        value = datetime.strptime(value, '%Y-%m-%d %H:%M:%S')
    return value.strftime(format)

templates.env.filters['format_date'] = format_date

@utils_router.get("/", response_class=HTMLResponse)
async def index(request: Request):
    current_date = datetime.now()
    return templates.TemplateResponse("index.html", {"request": request, "current_date": current_date})

# A utility function that filters data based on user-defined criteria
def filter_data(data):
    filtered_data = []
    for datum in data:
        if datum['label'] == 'positive':
            filtered_data.append(datum)
    return filtered_data

async def get_current_user(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)) -> UserRead:
    try:
        payload = jwt.decode(token, config.SECRET_KEY, algorithms=[config.ALGORITHM])
        user_id = payload.get("id")
    except jwt.PyJWTError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
    user = db.query(User).filter(User.id == user_id).first()
    if user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return UserRead.from_orm(user)