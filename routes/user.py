from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from models import User
from schemas import (
    UserCreate, 
    UserRead,
    UserUpdate,
)
from database import get_db

user_router = APIRouter()

@user_router.post("/", response_model=UserRead, status_code=status.HTTP_201_CREATED)
async def create_user(user: UserCreate, db: Session = Depends(get_db)):
    # Check if the username or email already exists
    existing_user = await User.get_user_by_email(user.email, db)
    if existing_user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Email already exists",
        )

    # Create a new User instance
    db_user = User(**user.dict())
    db_user.set_password(user.password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    # Convert the ORM object (User model) to the Pydantic object (UserRead)
    return UserRead.from_orm(db_user)

# read_user: Fetches a user by its ID.
@user_router.get("/{user_id}", response_model=UserRead)
async def read_user(user_id: int, db: Session = Depends(get_db)):
    user = db.query(User).filter(User.id == user_id).first()
    if user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User not found"
        )
    return UserRead.from_orm(user)

# update_user: Updates a user's information by its ID.
@user_router.put("/{user_id}", response_model=UserRead)
async def update_user(user_id: int, user: UserUpdate, db: Session = Depends(get_db)):
    db_user = db.query(User).filter(User.id == user_id).first()
    if db_user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User not found"
        )

    if user.username:
        existing_user = await User.get_user_by_username(user.username, db)
        if existing_user and existing_user.id != user_id:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Username already exists",
            )
        db_user.username = user.username

    if user.email:
        existing_user = await User.get_user_by_email(user.email, db)
        if existing_user and existing_user.id != user_id:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Email already exists",
            )
        db_user.email = user.email

    if user.password:
        db_user.set_password(user.password)

    db.commit()
    db.refresh(db_user)
    return UserRead.from_orm(db_user)

# delete_user: Deletes a user by its ID.
@user_router.delete("/{user_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_user(user_id: int, db: Session = Depends(get_db)):
    user = db.query(User).filter(User.id == user_id).first()
    if user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User not found"
        )
    db.delete(user)
    db.commit()
    return {}