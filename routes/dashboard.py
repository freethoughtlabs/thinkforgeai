from typing import List

from fastapi import APIRouter, Depends, HTTPException, Request
from fastapi.responses import HTMLResponse
from sqlalchemy.orm import Session
from fastapi.templating import Jinja2Templates

from models import Annotation, Model, DataSource, Task, Label
from database import get_db
from schemas import TaskRead, LabelRead

dashboard_router = APIRouter()
templates = Jinja2Templates(directory="templates")


@dashboard_router.get("/dashboard", response_class=HTMLResponse)
async def dashboard(request: Request, db: Session = Depends(get_db)):
    annotations = db.query(Annotation).filter_by(user_id=request.user.id).all()
    return templates.TemplateResponse("dashboard.html", {"request": request, "title": "Dashboard", "annotations": annotations})


@dashboard_router.get("/annotations", response_class=HTMLResponse)
async def annotations(request: Request, db: Session = Depends(get_db)):
    annotations = db.query(Annotation).all()
    return templates.TemplateResponse("annotations.html", {"request": request, "title": "Annotations", "annotations": annotations})


@dashboard_router.get("/models", response_class=HTMLResponse)
async def models(request: Request, db: Session = Depends(get_db)):
    models = db.query(Model).all()
    return templates.TemplateResponse("models.html", {"request": request, "title": "Models", "models": models})


@dashboard_router.get("/data_sources", response_class=HTMLResponse)
async def data_sources(request: Request, db: Session = Depends(get_db)):
    data_sources = db.query(DataSource).all()
    return templates.TemplateResponse("data_sources.html", {"request": request, "title": "Data Sources", "data_sources": data_sources})


@dashboard_router.get("/tasks", response_class=HTMLResponse)
async def tasks(request: Request, db: Session = Depends(get_db)):
    tasks = db.query(Task).all()
    return templates.TemplateResponse("tasks.html", {"request": request, "title": "Tasks", "tasks": tasks})


@dashboard_router.get("/labels", response_class=HTMLResponse)
async def labels(request: Request, db: Session = Depends(get_db)):
    labels = db.query(Label).all()
    return templates.TemplateResponse("labels.html", {"request": request, "title": "Labels", "labels": labels})
