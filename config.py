class Config(object):
    DEBUG = True
    TESTING = True
    DATABASE_URI = 'sqlite+aiosqlite:///thinkforgeai.db'
    SECRET_KEY = 'your-secret-key-here'
    PROPAGATE_EXCEPTIONS = True
