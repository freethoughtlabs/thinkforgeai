from datetime import datetime
from typing import Optional
from pydantic import BaseModel

class UserBase(BaseModel):
    id: int
    username: str
    email: str
    created_at: datetime

    class Config:
        orm_mode = True


class UserCreate(UserBase):
    password: str


class UserRead(UserBase):
    id: int
    username: str
    email: str

    class Config:
        orm_mode = True


class UserUpdate(UserBase):
    password: Optional[str] = None


class AnnotationBase(BaseModel):
    model_id: int
    task_id: int
    user_id: int
    text: str
    label: str
    text: Optional[str] = None
    label: Optional[str] = None


class AnnotationCreate(AnnotationBase):
    pass


class AnnotationRead(AnnotationBase):
    id: int
    created_at: Optional[datetime] = None
    updated_at: Optional[datetime] = None

    class Config:
        orm_mode = True
'''
class AnnotationOut(BaseModel):
    id: int
    model_id: int
    task_id: int
    user_id: int
    text: str
    label: str
    created_at: datetime
    updated_at: Optional[datetime] = None
'''
class AnnotationResponse(BaseModel):
    id: int
    model_id: int
    task_id: int
    user_id: int
    text: str
    label: str
    created_at: datetime

class AnnotationUpdate(AnnotationBase):
    text: Optional[str] = None
    label: Optional[str] = None

class ModelBase(BaseModel):
    name: str
    description: Optional[str] = None


class ModelCreate(ModelBase):
    pass


class ModelRead(ModelBase):
    id: int
    created_at: Optional[datetime] = None
    data_source_id: Optional[int] = None

    class Config:
        orm_mode = True


class ModelUpdate(ModelBase):
    pass


class DataSourceBase(BaseModel):
    name: str


class DataSourceCreate(DataSourceBase):
    pass


class DataSourceRead(DataSourceBase):
    id: int
    created_at: Optional[datetime] = None

    class Config:
        orm_mode = True


class DataSourceUpdate(DataSourceBase):
    pass


class TaskBase(BaseModel):
    name: str


class TaskCreate(TaskBase):
    pass


class TaskRead(TaskBase):
    id: int
    created_at: Optional[datetime] = None
    dataset_id: Optional[int] = None

    class Config:
        orm_mode = True


class TaskUpdate(TaskBase):
    pass


class LabelBase(BaseModel):
    name: str
    task_id: int


class LabelCreate(LabelBase):
    pass


class LabelRead(LabelBase):
    id: int
    created_at: Optional[datetime] = None

    class Config:
        orm_mode = True


class LabelUpdate(LabelBase):
    pass

class DatasetBase(BaseModel):
    name: str
    description: Optional[str] = None


class DatasetCreate(DatasetBase):
    pass


class DatasetRead(DatasetBase):
    id: int
    created_at: Optional[datetime] = None
    updated_at: Optional[datetime] = None

    class Config:
        orm_mode = True


class DatasetUpdate(DatasetBase):
    pass