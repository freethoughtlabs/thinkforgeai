from pydantic import BaseModel, Field, validator, constr

from fastapi import HTTPException, Depends
from models import User
from sqlalchemy.orm import Session
from database import get_db
import re

class UserForm(BaseModel):
    email: constr(regex=r'^[\w\.\+\-]+\@[\w]+\.[a-z]{2,3}$')

    @validator('email')
    def check_email(cls, email):
        if not re.match(cls.email.field_info.extra['regex'], email):
            raise ValueError('Invalid email address')
        return email


class LoginForm(BaseModel):
    email: constr(regex=r'^[\w\.\+\-]+\@[\w]+\.[a-z]{2,3}$')
    password: str = Field(..., min_length=8)
    remember_me: bool = False
    submit: str = 'Sign In'

    @validator('email')
    def validate_email(cls, email):
        if not re.match(cls.email.field_info.extra['regex'], email):
            raise ValueError('Invalid email address')
        return email


class RegistrationForm(BaseModel):
    username: str = Field(..., min_length=3, max_length=50)
    email: constr(regex=r'^[\w\.\+\-]+\@[\w]+\.[a-z]{2,3}$')
    password: str = Field(..., min_length=8)
    password2: str = Field(...)

    @validator('email')
    def validate_email(cls, email):
        if not re.match(cls.email.field_info.extra['regex'], email):
            raise ValueError('Invalid email address')
        return email

    async def validate_username(self, db: Session = Depends(get_db)):
        user = await User.get_user_by_username(self.username, db)
        if user is not None:
            raise HTTPException(status_code=400, detail='Please use a different username.')

    async def validate_email(self, db: Session = Depends(get_db)):
        user = await User.get_user_by_email(self.email, db)
        if user is not None:
            raise HTTPException(status_code=400, detail='Please use a different email address.')

    async def validate_password_match(self):
        if self.password != self.password2:
            raise HTTPException(status_code=400, detail='Passwords must match.')
