'''
To get the database up and running, you need to create the database 
tables based on your models.

To create the tables, simply run this script:

python create_tables.py
This will create the tables in your database based on the models defined 
in models.py.

Now your FastAPI app should be ready to run with the database set up. 
To run the app, use the following command:

uvicorn main:app --host 0.0.0.0 --port 8000 --reload

Or

python3 main.py
'''
import asyncio
from sqlalchemy.ext.asyncio import AsyncSession
from database import engine, async_session
from models import Base

async def create_tables():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)

if __name__ == "__main__":
    asyncio.run(create_tables())