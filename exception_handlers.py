from fastapi import Request, HTTPException
from fastapi.responses import JSONResponse
from pydantic import ValidationError
from fastapi.exceptions import RequestValidationError
from sqlalchemy.exc import DatabaseError

async def handle_exception(request: Request, exc: Exception):
    return JSONResponse(content={"detail": str(exc)}, status_code=500)

async def handle_validation_error(request: Request, exc: ValidationError):
    return JSONResponse(content={"detail": exc.errors()}, status_code=422)

async def handle_http_exception(request: Request, exc: HTTPException):
    return JSONResponse(content={"detail": exc.detail}, status_code=exc.status_code)

async def handle_request_validation_error(request: Request, exc: RequestValidationError):
    return JSONResponse(content={"detail": exc.errors()}, status_code=422)

async def handle_database_error(request: Request, exc: DatabaseError):
    return JSONResponse(content={"detail": "A database error occurred."}, status_code=500)
