"""
main.py: A FastAPI application for managing various resources related to a machine learning platform.

This application includes various routes for handling authentication, annotations, dashboard, data,
datasets, models, utilities, users, and an index route. The application is configured with CORS
middleware for handling Cross-Origin Resource Sharing and includes custom exception handling.

The FastAPI app instance is configured with a database, static files, and Jinja2 templates for
serving HTML content.

The application can be run using uvicorn, with the host and port specified in the environment
variables or by using the default values.
"""

from datetime import datetime
import os
from fastapi import FastAPI, HTTPException
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from pydantic import ValidationError
from sqlalchemy.exc import DatabaseError

from exception_handlers import (
    handle_exception,
    handle_validation_error,
    handle_http_exception,
    handle_request_validation_error,
    handle_database_error,
)

from routes import (
    auth_router,
    annotations_router,
    dashboard_router,
    data_router,
    dataset_router,
    model_router,
    utils_router,
    user_router,
    index_router,
)

from database import configure_database

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

# This will allow requests from any origin, but you can modify it to 
# only allow specific origins in a production environment.
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

configure_database(app)

app.include_router(auth_router, prefix='/auth', tags=["auth"])
app.include_router(annotations_router, prefix='/annotations', tags=["annotations"])
app.include_router(dashboard_router, prefix='/dashboard', tags=["dashboard"])
app.include_router(data_router, prefix='/data', tags=["data"])
app.include_router(dataset_router, prefix='/dataset', tags=["dataset"])
app.include_router(model_router, prefix='/model', tags=["model"])
app.include_router(utils_router, prefix='/utils', tags=["utils"])
app.include_router(user_router, prefix='/user', tags=["user"])
app.include_router(index_router, prefix='/index', tags=["index"])

app.add_exception_handler(Exception, handle_exception)
app.add_exception_handler(ValidationError, handle_validation_error)
app.add_exception_handler(HTTPException, handle_http_exception)
app.add_exception_handler(RequestValidationError, handle_request_validation_error)
app.add_exception_handler(DatabaseError, handle_database_error)

if __name__ == "__main__":
    import uvicorn

    database_url = os.environ.get("DATABASE_URL")
    uvicorn.run(app, host="0.0.0.0", port=8000)