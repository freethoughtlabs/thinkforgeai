from typing import Dict
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime
from sqlalchemy import create_engine, Column, Integer, String, DateTime, ForeignKey, JSON, Select
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import relationship, Session
from schemas import AnnotationCreate

Base = declarative_base()

# A model that represents a machine learning model
class Model(Base):
    __tablename__ = "model"
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    created_at = Column(DateTime, default=datetime.utcnow)
    data_source_id = Column(Integer, ForeignKey("data_source.id"))
    data_source = relationship("DataSource", back_populates="models")
    annotations = relationship("Annotation", back_populates="model")
    

# A model that represents a data source
class DataSource(Base):
    __tablename__ = "data_source"
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    created_at = Column(DateTime, default=datetime.utcnow)
    data_type = Column(String(255))
    path = Column(String(255))
    models = relationship("Model", back_populates="data_source")


# A model that represents a user of the application
class User(Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True)
    username = Column(String(64), index=True, unique=True)
    email = Column(String(120), index=True, unique=True, nullable=False)
    password_hash = Column(String(128), nullable=False)
    created_at = Column(DateTime, default=datetime.utcnow)
    annotations = relationship("Annotation", back_populates="user")

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
    
    '''
    The set_password and check_password methods are used to manage the user's 
    password. The set_password method takes a plain-text password as input and 
    tores its hash in the password_hash attribute. The check_password method is 
    used to verify if a provided password matches the user's stored password hash.
    '''

    @classmethod
    async def get_user_by_username(cls, username: str, db: AsyncSession):
        async with db.begin():
            return await db.execute(Select(cls).where(cls.username == username))

    @classmethod
    async def get_user_by_email(cls, email: str, db: AsyncSession):
        async with db.begin() as session:
            return await session.execute(Select(User).where(User.email == email)).scalar_one_or_none()
    
    '''
    The class methods get_user_by_username and get_user_by_email are defined to 
    retrieve a user object by their username or email, respectively. These methods 
    take the username/email and a database session object as input and return the 
    corresponding user object if it exists in the database, otherwise they return None. 
    These methods are declared as class methods because they don't need an instance of 
    the class to be called, and they are prefixed with the async keyword because they 
    are intended to be used with an asynchronous database driver.
    '''

# A model that represents an annotation made by a user
"""
This class is a model that represents annotations in the FastAPI application. It has an 
auto-incrementing primary key id, text and label attributes that store the annotated 
text and its associated label, respectively. The user_id attribute is a foreign key that 
links the annotation to the user who created it, and the user attribute is a SQLAlchemy 
relationship that allows you to access the user object associated with an annotation. 
The created_at attribute stores the date and time when the annotation was created.
"""

class Annotation(Base):
    __tablename__ = "annotation"
    id = Column(Integer, primary_key=True)
    model_id = Column(Integer, ForeignKey("model.id"))
    task_id = Column(Integer, ForeignKey("task.id"), nullable=False)
    user_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    data = Column(JSON, nullable=False)
    user = relationship("User", back_populates="annotations")
    text = Column(String(255))
    label = Column(String(255))
    created_at = Column(DateTime, default=datetime.utcnow)

    model = relationship("Model", back_populates="annotations")
    task = relationship("Task", back_populates="annotations")

    def __init__(self, task_id: int, user_id: int, data: Dict, text: str, label: str):
        self.task_id = task_id
        self.user_id = user_id
        self.data = data
        self.text = text
        self.label = label

    def __repr__(self):
        return '<Annotation {}>'.format(self.id)
    
    @classmethod
    def create(cls, db: Session, annotation: AnnotationCreate, user_id: int) -> "Annotation":
        annotation = cls(
            task_id=annotation.task_id,
            user_id=user_id,
            data=annotation.data,
            text=annotation.text,
            label=annotation.label
        )
        db.add(annotation)
        db.commit()
        db.refresh(annotation)
        return annotation

'''
In the models.py file, db is not declared. It is typically passed as an argument to methods that 
require access to the database, such as query methods or transactional methods that insert or 
update data. The Session object from SQLAlchemy is typically used to manage database transactions 
and to interact with the database. For example, in the create method of the Annotation model, the 
db argument is used to add the new annotation to the session, commit the transaction and refresh 
the instance.
'''

# A model that represents a dataset
class Dataset(Base):
    __tablename__ = "dataset"
    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    description = Column(String(255))
    file_path = Column(String(255), nullable=False)
    user_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    user = relationship("User", backref="datasets")
    created_at = Column(DateTime, default=datetime.utcnow)

class Comment(Base):
    __tablename__ = "comment"
    id = Column(Integer, primary_key=True)
    text = Column(String(255), nullable=False)
    user_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    user = relationship("User", backref="comments")
    created_at = Column(DateTime, default=datetime.utcnow)

    def __repr__(self):
        return f'<Comment {self.text[:20]}>'


class Task(Base):
    __tablename__ = "task"
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    created_at = Column(DateTime, default=datetime.utcnow)
    dataset_id = Column(Integer, ForeignKey("dataset.id"))
    annotations = relationship("Annotation", back_populates="task")

    def __repr__(self):
        return f'<Task {self.name}>'
    
    '''
    The __repr__ method is used to provide a string representation of an object. 
    It is often useful for debugging and logging purposes. In this case, the 
    __repr__ method is defined for the Task class to provide a string representation 
    of a Task object that includes its name. This can be helpful in understanding 
    which Task object is being referred to when working with a large number of Task 
    objects.
    '''


class Label(Base):
    __tablename__ = "label"
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    task_id = Column(Integer, ForeignKey("task.id"), nullable=False)
    task = relationship("Task", back_populates="labels")
    created_at = Column(DateTime, default=datetime.utcnow)

    def __repr__(self):
        return f'<Label {self.name}>'

Task.labels = relationship("Label", order_by=Label.id, back_populates="task")